create table if not exists Buildings (
	BuildingID int primary key,
	BuildingName varchar(100),
	BuildingAddress varchar(500)
);

create table if not exists Apartments (
	ApartmentID int primary key,
	AparmtentAdress varchar(100),
	BuildingID int references Buildings(BuildingId)
);

create table if not exists Tenants (
	TenantID int primary key,
	TenantName varchar(100),
	TenantAddress varchar(500)
);

create table if not exists TenantApartments (
	TenantID int references Tenants(TenantID),
	ApartmentID int references Apartments(ApartmentID),
	primary key (TenantID, ApartmentID)
);

create table if not exists Requests (
	RequestID int primary key,
	Status varchar(100),
	ApartmentID int references Apartments(ApartmentID),
	Description varchar(500)
);

insert into Buildings values
(0, 'First Building', 'First St. 1'),
(1, 'Second Building', 'First St. 2'),
(2, 'Third Building', 'Second St. 3');

insert into Apartments values
(0, 'First St. 1', 0),
(1, 'First St. 1', 0),
(2, 'First St. 1', 0),
(3, 'First St. 2', 1),
(4, 'First St. 2', 1),
(5, 'First St. 2', 1);

insert into Requests values
(0, 'Open', 1, 'hello'),
(1, 'Open', 1, 'hello also'),
(2, 'Rejected', 1, 'rude hello');
