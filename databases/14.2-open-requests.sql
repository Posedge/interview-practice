select b.buildingid, min(b.buildingname), count(r)
from buildings b left join apartments a
	on b.buildingid = a.buildingid
left join requests r
	on a.apartmentid = r.apartmentid
	and r.status = 'Open'
group by b.buildingid ;