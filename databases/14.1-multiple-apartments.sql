select Tenants.TenantID, min(Tenants.TenantName), count(TenantApartments.ApartmentID)
from Tenants, TenantApartments
group by Tenants.TenantID
having count(TenantApartments.ApartmentID) > 0