#ifndef NODE_H
#define NODE_H

#include <iostream>

using namespace std;

class Node {
    public:
    Node *left;
    Node *right;
    int data;

    Node(int d);
    Node(int d, Node *l, Node *r);
    void print(ostream& os) const;
    Node *deep_copy();

    private:
    void print(ostream& os, int depth) const;

    friend ostream& operator<<(ostream& os, const Node& node);
};

#endif
