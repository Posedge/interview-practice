#include "node.h"
#include <iostream>

using namespace std;

Node::Node(int d) : data(d) {}

Node::Node(int d, Node *l, Node *r) : data(d), left(l), right(r) {};

Node *Node::deep_copy() {
    Node *l = NULL;
    if (left) {
        l = left->deep_copy();
    }
    Node *r = NULL;
    if (right) {
        r = right->deep_copy();
    }
    // data is a primitive type, no need to clone
    return new Node(data, l, r);
}

void Node::print(ostream& os, int depth) const {
    for (int i = 0; i < depth; i++) {
        os << " > ";
    }
    os << "{" << data << "}" << endl;
    if (left) {
        left->print(os, depth+1);
    }
    if (right) {
        right->print(os, depth+1);
    }
}

void Node::print(ostream& os) const {
    print(os, 0);
}

ostream& operator<<(ostream& os, const Node& node) {
    node.print(os);
    return os;
}
