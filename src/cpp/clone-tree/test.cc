#include <iostream>
#include "node.h"

using namespace std;

void report_test(bool passed, char *message) {
    if (passed) {
        cout << message << ": PASS" << endl;
    } else {
        cout << message << ": FAIL" << endl;
    }
}

int main(int argc, char **argv) {
    Node *tree = new Node(7,
            new Node(3,
                NULL,
                new Node(4)),
            new Node(12,
                new Node(8),
                new Node(14)));
    cout << "Printing the tree: " << endl;
    cout << *tree << endl;

    Node *clone = tree->deep_copy();
    cout << "Printing the clone: " << endl;
    cout << *tree << endl;

    report_test(clone != tree, "Root is not the same");
    report_test(clone->data == tree->data, "Root is equal");
    report_test(clone->left != tree->left, "Nested node is not the same");
    report_test(clone->left->data == tree->left->data, "Nested node is equal");
}
