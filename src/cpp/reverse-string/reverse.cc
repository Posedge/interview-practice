#include <iostream>

using namespace std;

void reverse(char *str);

int main(int argc, char **argv) {
    char str[] = "hello world blah";
    reverse(str);
    cout << str << endl;
}

// TODO declare above main
void reverse(char *str) {
    if (str == nullptr) return;
    if (str[0] == '\0') return;

    char *i = str;
    while (*i != '\0') i++;
    i--;

    while (str < i) {
        // swap
        char tmp = *str;
        *str = *i;
        *i = tmp;
        i--;
        str++;
    }
}
