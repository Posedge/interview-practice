package com.company.rankfromstream;

import java.util.stream.Stream;

public class RankFromStream {

    public static void main(String... args) {
        RankFromStream main = new RankFromStream();
        Stream.of(5, 1, 4, 4, 5, 9, 7, 13, 3).forEach(main::track);
        main.tree.print(0);
        System.out.println(main.getRankOfNumber(1));
        System.out.println(main.getRankOfNumber(3));
        System.out.println(main.getRankOfNumber(4));
    }

    Node insert(Node root, int data) {
        if (root == null) {
            return new Node(data);
        }
        if (root.data >= data) {
            root.left = insert(root.left, data);
            root.left.parent = root;
            root.weight++;
        } else {
            root.right = insert(root.right, data);
            root.right.parent = root;
            root.weight++;
        }
        return root;
    }

    Node tree;

    void track(int x) {
        tree = insert(tree, x);
    }

    int getRankOfNumber(int x) {
        Node node = tree;
        if (node == null) return 0;

        // find next smaller number
        while (node.data != x) {
            if (node.data > x) {
                if (node.left != null) {
                    node = node.left;
                } else {
                    break;
                }
            } else {
                if (node.right != null) {
                    node = node.right;
                } else {
                    break;
                }
            }
        }

        // move to next smaller number?
        while (node.data > x) {
            if (node.parent == null) {
                // smaller than all tracked numbers
                return 0;
            }
            node = node.parent;
        }

        int weightLeft = node.left == null ? 0 : node.left.weight;
        // within sub-tree
        int rank = weightLeft;

        while (node.parent != null) {
            if (node.parent.data >= node.data) {
                node = node.parent;
                continue;
            }
            int leftWeight = node.parent.left == null ? 0 : node.parent.left.weight;
            rank += leftWeight + 1;
            node = node.parent;
        }
        return rank;
    }

}

class Node {
    int data;
    int weight;
    Node left;
    Node right;
    Node parent;

    Node(int data) {
        this.data = data;
        this.weight = 1;
        this.left = null;
        this.right = null;
        this.parent = null;
    }

    void print(int depth) {
        for (int i = 0; i < depth; i++) {
            System.out.print("> ");
        }
        System.out.println(data + " (weight=" + weight + ")");
        if (left != null) left.print(depth + 1);
        if (right != null) right.print(depth + 1);
    }
}
