package com.company;

public class MagicIndex {
    public static void main(String[] args) {
        int[] arr = {-7, -5, -1, 2, 4, 7};
        System.out.println(new MagicIndex().magicIndex(arr));
    }

    int magicIndex(int[] arr) {
        return magicIndex(arr, 0, arr.length);
    }

    private int magicIndex(int[] arr, int start, int end) {
        int size = end - start;
        assert size > 0;

        if (size == 1 || size == 2) {
            if (arr[start] == start) {
                return start;
            } else if (size == 2 && arr[start + 1] == start + 1) {
                return start + 1;
            } else {
                return -1;
            }
        }

        int mid = start + size / 2;
        if (arr[mid] == mid) {
            return mid;
        } else if (arr[mid] < mid) {
            return magicIndex(arr, mid + 1, end);
        } else {
            return magicIndex(arr, start, mid);
        }
    }

}
