package com.company.multithreadfizzbuzz;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.function.Predicate;

public class Main {
    int count = 0;
    static final int N = 100;

    public static void main(String... args) throws InterruptedException {
        Main m = new Main();
        Thread fizz = new FizzThread("fizz", m, i -> i % 3 == 0 && i % 5 != 0, i -> "Fizz");
        Thread buzz = new FizzThread("buzz", m, i -> i % 3 != 0 && i % 5 == 0, i -> "Buzz");
        Thread fizzbuzz = new FizzThread("fizzbuzz", m, i -> i % 3 == 0 && i % 5 == 0, i -> "FizzBuzz");
        Thread numbers = new FizzThread("numbers", m, i -> i % 3 != 0 && i % 5 != 0, Object::toString);
        fizz.start();
        buzz.start();
        fizzbuzz.start();
        numbers.start();
        fizz.join();
        buzz.join();
        fizzbuzz.join();
        numbers.join();
    }
}

class FizzThread extends Thread {
    final Main main;
    final Predicate<Integer> printPredicate;
    final Function<Integer, String> format;

    public FizzThread(String name, Main main, Predicate<Integer> printPredicate, Function<Integer, String> format) {
        super(name);
        this.main = main;
        this.printPredicate = printPredicate;
        this.format = format;
    }

    @Override
    public void run() {
        synchronized (main) {
            print();
        }
    }

    void print() {
        while (main.count < Main.N) {
            while (printPredicate.negate().test(main.count) && main.count < Main.N) {
                try {
                    main.wait();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            if (main.count < Main.N) {
                System.out.println(format.apply(main.count));
                main.count++;
            }
            main.notifyAll();
        }
    }
}
