package com.company;

import java.util.Arrays;

public class MissingTwo {

    public static void main(String... args) {
        MissingTwo m = new MissingTwo();
        int[] numbers = new int[]{1, 3, 4, 5, 6, 8, 9, 10};
        int[] missing = m.missingTwo(numbers, 10);
        System.out.printf("missing numbers: %d, %d\n", missing[0], missing[1]);
    }

    int[] missingTwo(int[] numbers, int n) {
        assert n > 0;
        assert numbers != null && numbers.length > 0 && numbers.length + 2 == n;

        int expectedSum = 0;
        int expectedSumSquares = 0;
        for (int i = 1; i <= n; i++) {
            expectedSum += i;
            expectedSumSquares += i*i;
        }

        int sum = 0;
        int sumSquares = 0;
        for (int number : numbers) {
            sum += number;
            sumSquares += number * number;
        }

        int x = expectedSum - sum;
        int y = expectedSumSquares - sumSquares;

        // midnight formula variables
        int a = 2;
        int b = -2 * x;
        int c = x*x - y;
        int[] solutions = midnightFormula(a, b, c);
        assert solutions[0] + solutions[1] == x;
        return solutions;
    }

    int[] midnightFormula(int a, int b, int c) {
        int termUnderRoot = b*b - 4*a*c;
        if (termUnderRoot < 0) {
            throw new IllegalArgumentException("only equations with >= 1 solutions are supported");
        }

        int root = (int) Math.sqrt(termUnderRoot);
        if (root == 0) {
            return new int[]{-b / (2*a)};
        }
        return new int[]{
                (-b + root) / (2*a),
                (-b - root) / (2*a)
        };
    }

}
