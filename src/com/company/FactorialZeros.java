package com.company;

public class FactorialZeros {
    public static void main(String... argv) {
        System.out.println(new FactorialZeros().factorialZeros(10));
        System.out.println(new FactorialZeros().factorialZeros(15));
        System.out.println(new FactorialZeros().factorialZeros(20));
        System.out.println(new FactorialZeros().factorialZeros(25));
    }

    int factorialZeros(int n) {
        assert n >= 0;
        if (n == 0) return 0; // 0! = 1
        int twos = 0;
        int fives = 0;
        for (int i = 1; i <= n; i++) {
            int j = i;
            int k = i;
            while (j % 2 == 0) {
                twos++;
                j = j / 2;
            }
            while (k % 5 == 0) {
                fives++;
                k = k / 5;
            }
        }
        return Math.min(twos, fives);
    }
}
