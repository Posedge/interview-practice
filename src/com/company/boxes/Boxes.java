package com.company.boxes;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;

public class Boxes {
    public static void main(String[] args) {
        Box[] boxes = {
                new Box(7, 3, 9), new Box(6, 8, 7), new Box(4, 5, 11), new Box(2, 2, 3),
                new Box(1, 1, 2)
        };
        int mh = new Boxes().maxHeight(boxes);
        System.out.println(mh);
    }

    public int maxHeight(Box[] boxes) {
        Arrays.sort(boxes, Comparator.comparingInt(box -> box.width));
        int[] memo = new int[boxes.length];
        return maxHeight(boxes, boxes.length - 1, memo);
    }

    // @Memoize // ...implemented manually
    private int maxHeight(Box[] boxes, int i, int[] memo) {
        if (i < 0) return 0;
        if (memo[i] > 0) return memo[i];

        int skip = maxHeight(boxes, i - 1, memo);

        Box box = boxes[i];
        int j;
        for (j = i - 1; j >= 0; j--) {
            Box next = boxes[j];
            if (next.width < box.width && next.depth < box.depth && next.height < box.height) {
                break;
            }
        }
        int subStackHeight = j < 0 ? 0 : maxHeight(boxes, j, memo);
        int pick = box.height + subStackHeight;

        memo[i] = Math.max(skip, pick);
        return memo[i];
    }

    public static class Box {
        final int width;
        final int depth;
        final int height;

        public Box(int width, int depth, int height) {
            this.width = width;
            this.depth = depth;
            this.height = height;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Box box = (Box) o;
            return width == box.width && height == box.height && depth == box.depth;
        }

        @Override
        public int hashCode() {
            return Objects.hash(width, height, depth);
        }
    }
}
