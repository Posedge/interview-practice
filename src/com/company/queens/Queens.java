package com.company.queens;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

public class Queens {

    public static final int BOARD_SIZE = 4;

    public static void main(String[] args) {
        new Queens().queens();
    }

    void queens() {
        queens(new ArrayList<>(), new boolean[BOARD_SIZE], new boolean[2*BOARD_SIZE-1], new boolean[2*BOARD_SIZE-1]);
    }

    private void queens(List<Spot> board, boolean[] cols, boolean[] diags, boolean[] reverseDiags) {
        int row = board.size();
        if (row == BOARD_SIZE) {
            printBoard(board);
            return;
        }

        for (int col = 0; col < BOARD_SIZE; col++) {
            if (cols[col]) continue;
            int diag = col - row;
            int diag2 = col + row;
            if (diags[diag + BOARD_SIZE - 1]) continue;
            if (reverseDiags[diag2]) continue;

            boolean[] cols2 = cols.clone();
            boolean[] diags2 = diags.clone();
            boolean[] reverseDiags2 = reverseDiags.clone();
            cols2[col] = true;
            diags2[diag+BOARD_SIZE-1] = true;
            reverseDiags2[diag2] = true;
            board.add(new Spot(row, col));
            queens(board, cols2, diags2, reverseDiags2);
            board.remove(board.size()-1);
        }
    }

    private void printBoard(List<Spot> board) {
        HashSet<Spot> queens = new HashSet<>(board);
        for (int row = 0; row < BOARD_SIZE; row++) {
            for (int col = 0; col < BOARD_SIZE; col++) {
                if (queens.contains(new Spot(row, col))) {
                    System.out.print('*');
                } else {
                    System.out.print('.');
                }
            }
            System.out.print('\n');
        }

        System.out.print("\n\n");
    }

    public static class Spot {
        final int row;
        final int col;

        public Spot(int row, int col) {
            this.row = row;
            this.col = col;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Spot spot = (Spot) o;
            return row == spot.row && col == spot.col;
        }

        @Override
        public int hashCode() {
            return Objects.hash(row, col);
        }
    }
}
