package com.company;

class Node {
    final int data;
    final Node left;
    final Node right;

    Node(int data, Node left, Node right) {
        this.data = data;
        this.left = left;
        this.right = right;
    }

    Node(int data) {
        this.data = data;
        this.left = null;
        this.right = null;
    }

    void print(int depth) {
        for (int i = 0; i < depth; i++) {
            System.out.print("> ");
        }
        System.out.println(data);
        if (left != null) left.print(depth + 1);
        if (right != null) right.print(depth + 1);
    }
}
