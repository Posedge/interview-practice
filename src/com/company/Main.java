package com.company;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class Main {

    public static void main(String[] args) {
        Node tree = new Node(7,
                new Node(-3),
                new Node(4,
                        new Node(-3,
                                new Node(8,
                                        null,
                                        new Node(2)),
                                new Node(7)),
                        new Node(5)));
        tree.print(0);
        System.out.println("paths w/ sum 9: " + new Main().pathsWithSum(tree, 9));
    }

    int pathsWithSum(Node root, int target) {
        return paths(root, target, 0, new HashMap<>());
    }

    int paths(Node tree, int target, int runningSum, HashMap<Integer, HashSet<Node>> prefixSums) {
        // base case
        if (tree == null) {
            return 0;
        }

        // paths ending here
        runningSum += tree.data;
        int nPaths = 0;
        if (target == runningSum) {
            // path from root to here
            nPaths++;
        }

        Set<Node> prefixNodes = prefixSums.get(runningSum - target);
        if (prefixNodes != null) nPaths += prefixNodes.size();

        // recursive call
        if (!prefixSums.containsKey(runningSum)) {
            HashSet<Node> singletonSet = new HashSet<>();
            singletonSet.add(tree);
            prefixSums.put(runningSum, singletonSet);
        } else {
            prefixSums.get(runningSum).add(tree);
        }
        nPaths += paths(tree.left, target, runningSum, prefixSums);
        nPaths += paths(tree.right, target, runningSum, prefixSums);

        // clean up! since the map is shared state.
        prefixSums.get(runningSum).remove(tree);
        return nPaths;
    }

}
