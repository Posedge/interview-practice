package com.company.randomnodetree;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        Main m = new Main();
        Node tree = null;
        tree = m.insert(tree, 3);
        tree = m.insert(tree, 1);
        tree = m.insert(tree, 8);
        tree = m.insert(tree, 7);
        tree = m.insert(tree, 5);
        tree = m.insert(tree, 10);
        tree = m.remove(tree, 8);
        tree.print(0);

        for (int i = 0; i < 10; i++) {
            System.out.println("Random node: " + m.getRandomNode(tree).value);
        }
    }

    Node insert(Node root, int value) {
        if (root == null) {
            return new Node(1, value, null, null);
        }
        if (value <= root.value) {
            root.size++;
            root.left = insert(root.left, value);
        } else {
            root.size++;
            root.right = insert(root.right, value);
        }
        return root;
    }

    Node remove(Node root, int value) {
        if (root == null) return null;
        if (value == root.value) {
            // remove this node
            if (root.left == null && root.right == null) {
                // remove leaf
                return null;
            } else if (root.right == null) {
                return root.left;
            } else if (root.left == null) {
                return root.right;
            } else {
                // both non-null
                int oldLeftValue = root.left.value;
                root.left = remove(root.left, oldLeftValue);
                root.value = oldLeftValue;
                root.size--;
            }
        } else if (value < root.value && root.left != null) {
            root.left = remove(root.left, value);
            int leftSize = root.left != null ? root.left.size : 0;
            int rightSize = root.right != null ? root.right.size : 0;
            root.size = leftSize + rightSize + 1;
        } else if(value > root.value && root.right != null) {
            root.right = remove(root.right, value);
            int leftSize = root.left != null ? root.left.size : 0;
            int rightSize = root.right != null ? root.right.size : 0;
            root.size = leftSize + rightSize + 1;
        }
        return root;
    }

    boolean sample(float probability) {
        assert probability >= 0f && probability <= 1f;
        return new Random().nextFloat() <= probability;
    }

    Node getRandomNode(Node root) {
        assert root != null;
        float leftSize = root.left == null ? 0f : root.left.size;
        float rightSize = root.right == null ? 0f : root.right.size;
        if (sample(1f / (leftSize + rightSize + 1f))) {
            return root;
        } else if (sample(leftSize / (leftSize + rightSize))) {
            return getRandomNode(root.left);
        } else {
            return getRandomNode(root.right);
        }
    }
}

class Node {
    int size;
    int value;
    Node left;
    Node right;

    public Node(int size, int value, Node left, Node right) {
        this.size = size;
        this.value = value;
        this.left = left;
        this.right = right;
    }

    void print(int depth) {
        for (int i = 0; i < depth; i++) {
            System.out.print("> ");
        }
        System.out.println(value + " (size=" + size + ")");
        if (left != null) left.print(depth + 1);
        if (right != null) right.print(depth + 1);
    }
}

